# ggez-demo

A demo of the [ggez](https://github.com/ggez/ggez) game engine.

May or may not grossly overuse features.

Image generated with `rainbow-color` and `large-window` features.

![Image of the result of running the program for a while](/image.png?raw=true)
