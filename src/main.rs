use ggez::{conf, event, graphics::*, nalgebra as na, Context, ContextBuilder, GameResult};
use rand::prelude::*;
use std::{env, path};

type Point2 = na::Point2<f32>;

#[cfg(not(any(
    feature = "tiny-window",
    feature = "small-window",
    feature = "large-window",
    feature = "massive-window"
)))]
const WIDTH: f32 = 400.0;
#[cfg(feature = "tiny-window")]
const WIDTH: f32 = 200.0;
#[cfg(feature = "small-window")]
const WIDTH: f32 = 300.0;
#[cfg(feature = "large-window")]
const WIDTH: f32 = 500.0;
#[cfg(feature = "massive-window")]
const WIDTH: f32 = 600.0;

#[cfg(not(any(
    feature = "tiny-window",
    feature = "small-window",
    feature = "large-window",
    feature = "massive-window"
)))]
const HEIGHT: f32 = 400.0;
#[cfg(feature = "tiny-window")]
const HEIGHT: f32 = 200.0;
#[cfg(feature = "small-window")]
const HEIGHT: f32 = 300.0;
#[cfg(feature = "large-window")]
const HEIGHT: f32 = 500.0;
#[cfg(feature = "massive-window")]
const HEIGHT: f32 = 600.0;

#[cfg(not(any(
    feature = "tiny-diff",
    feature = "small-diff",
    feature = "large-diff",
    feature = "massive-diff"
)))]
const MAX_DIFF: f32 = 3.0;
#[cfg(feature = "tiny-diff")]
const MAX_DIFF: f32 = 2.0;
#[cfg(feature = "small-diff")]
const MAX_DIFF: f32 = 2.5;
#[cfg(feature = "large-diff")]
const MAX_DIFF: f32 = 3.5;
#[cfg(feature = "massive-diff")]
const MAX_DIFF: f32 = 4.0;

#[cfg(not(any(
    feature = "tiny-point",
    feature = "small-point",
    feature = "large-point",
    feature = "massive-point"
)))]
const POINT_RADIUS: f32 = 3.0;
#[cfg(feature = "tiny-point")]
const POINT_RADIUS: f32 = 2.0;
#[cfg(feature = "small-point")]
const POINT_RADIUS: f32 = 2.5;
#[cfg(feature = "large-point")]
const POINT_RADIUS: f32 = 3.5;
#[cfg(feature = "massive-point")]
const POINT_RADIUS: f32 = 4.0;

#[cfg(feature = "rainbow-color")]
/// Create a color somewhere in the rainbow with the position `pos`.
/// (See https://krazydad.com/tutorials/makecolors.php)
fn rainbow(pos: usize, frequency: f32, phase: f32) -> Color {
    use std::f32::consts::PI;

    let center: f32 = 0.5;
    let width: f32 = 0.5;

    Color::new(
        (frequency * pos as f32 + ((2.0 * PI) * 1.0 / 3.0) + phase).sin() * width + center,
        (frequency * pos as f32 + 0.0 + phase).sin() * width + center,
        (frequency * pos as f32 + ((2.0 * PI) * 2.0 / 3.0) + phase).sin() * width + center,
        1.0,
    )
}

struct State {
    rng: ThreadRng,
    frames: usize,
    point: Point2,
    circle: Mesh,
}

impl State {
    pub fn new(ctx: &mut Context) -> GameResult<Self> {
        Ok(Self {
            rng: rand::thread_rng(),
            frames: 0,
            point: Point2::new(WIDTH / 2.0, HEIGHT / 2.0),
            circle: MeshBuilder::new()
                .circle(
                    DrawMode::fill(),
                    Point2::new(0.0, 0.0),
                    POINT_RADIUS,
                    1.0,
                    WHITE,
                )
                .build(ctx)?,
        })
    }
}

impl event::EventHandler for State {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        self.point[0] += (self.rng.gen::<f32>() * MAX_DIFF) - MAX_DIFF / 2.0;
        self.point[1] += (self.rng.gen::<f32>() * MAX_DIFF) - MAX_DIFF / 2.0;

        if self.point[0] < 0.0 {
            self.point[0] += WIDTH;
        }

        if self.point[0] > WIDTH {
            self.point[0] -= WIDTH;
        }

        if self.point[1] < 0.0 {
            self.point[1] += HEIGHT;
        }

        if self.point[1] > HEIGHT {
            self.point[1] -= HEIGHT;
        }

        self.frames += 1;

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        draw(
            ctx,
            &self.circle,
            DrawParam::new()
                .color({
                    #[cfg(not(any(feature = "rainbow-color", feature = "random-color")))]
                    {
                        WHITE
                    }

                    #[cfg(feature = "rainbow-color")]
                    {
                        rainbow(self.frames, 0.03, 0.0)
                    }

                    #[cfg(feature = "random-color")]
                    {
                        Color::new(
                            self.rng.gen::<f32>(),
                            self.rng.gen::<f32>(),
                            self.rng.gen::<f32>(),
                            1.0,
                        )
                    }
                })
                .dest(self.point),
        )?;

        present(ctx)
    }
}

fn main() -> GameResult<()> {
    let resource_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        path::PathBuf::from("./resources")
    };

    let (mut ctx, mut event_loop) = ContextBuilder::new("ggez-demo", "jakbyte")
        .window_setup(conf::WindowSetup::default().title("ggez demo"))
        .window_mode(conf::WindowMode::default().dimensions(WIDTH, HEIGHT))
        .add_resource_path(resource_dir)
        .build()?;
    let mut state = State::new(&mut ctx)?;

    event::run(&mut ctx, &mut event_loop, &mut state)
}
